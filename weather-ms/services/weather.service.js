const OpenWeatherMapLib = require('../libs/openWeatherMap');
const { error } = require('../libs/errors');
const { getCurrentWeatherFromDB, addCurrentWeatherToDB, getLocationMonthlyMisuration } = require('../db/helpers');
const { calculateMonthlyAverage, convertUnitDegreesTemperature, getMonth } = require('../utils');

const _retrieveWeather = async ({ location }) => {
  // Check if the measurements already exists (and is recent) on our db
  const weatherDB = await getCurrentWeatherFromDB({ location });
  if (weatherDB) return weatherDB;

  // If It doesn't exist call the third part API to retrieve the current weather
  const weather = await OpenWeatherMapLib.getWeatherByCityName({ location });
  if (!weather) throw error('MT', null, 400);

  // Save the measurements on db
  const response = await addCurrentWeatherToDB({ weather, location });

  return response;
};

const _retrieveMonthlyTemperatureAverage = async ({ location, month, unit }) => {
  const result = await getLocationMonthlyMisuration({ month, location });
  const monthlyAverage = calculateMonthlyAverage(result);
  const temperature = convertUnitDegreesTemperature(monthlyAverage, unit);

  return temperature;
};

/**
 *
 * GET LIVE WEATHER
 */
const getLiveWeather = async (req) => {
  const { location, unit } = req.query;
  if (!location) throw error('ML', null, 400);

  const response = await _retrieveWeather({ location });

  return {
    location: response.location,
    date: response.date,
    temperature: convertUnitDegreesTemperature(response.temperature, unit),
    unit: unit || 'K',
  };
};

/**
 *
 * GET AVERAGE TEMPERATURE BY LOCATION AND MONTH
 */
const getWeatherAverage = async (req) => {
  const { location, month, unit } = req.query;

  if (!location) throw error('ML', null, 400);
  if (!month) throw error('MM', null, 400);
  const monthNumber = getMonth(month);
  if (!monthNumber) throw error('IM', null, 400);

  const temperature = await _retrieveMonthlyTemperatureAverage(
    { location, month: monthNumber, unit },
  );
  return {
    temperatureAverage: temperature,
    unit: unit || 'K',
  };
};

module.exports = {
  getLiveWeather,
  getWeatherAverage,
};
