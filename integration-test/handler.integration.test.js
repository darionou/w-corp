const axios = require('axios');
const steps = require('./utils/steps');
// const init = require('./utils/init');

describe('When we invoke the hello function', () => {
  // beforeAll(() => {
  //   init();
  // });
  test('With a name', async () => {
    const result = await steps.invokeGetWeather('Rotterdam');
    expect(result.statusCode).toBe(200);
  });

  test('With a name and unit', async () => {
    const result = await steps.invokeGetWeatherWithUnit('Rotterdam', 'C');
    expect(result.statusCode).toBe(200);
    expect(result.body.unit).toBe('C');
    expect(result.body.location).toBe('Rotterdam');
  });
});
