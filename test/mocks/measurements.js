const { ObjectID } = require('mongodb');

const measurements = [
  {
    _id: ObjectID('5fcaaf6295d56849214d6be7'),
    temperature: 279.17,
    date: '2020-12-02T23:55:09.272+00:00',
    location: 'Rotterdam',
  },
  {
    _id: ObjectID('5fcaaf6295d56849214d6be8'),
    temperature: 285.17,
    date: '2020-12-03T23:55:09.272+00:00',
    location: 'Rotterdam',
  },
  {
    _id: ObjectID('5fcaaf6295d56849214d6be9'),
    temperature: 282.17,
    date: '2020-12-04T23:55:09.272+00:00',
    location: 'Rotterdam',
  },
  {
    _id: ObjectID('5fcaaf6295d56849214d6bf1'),
    temperature: 290.17,
    date: '2020-12-05T23:55:09.272+00:00',
    location: 'Rotterdam',
  },
];

module.exports = measurements;
