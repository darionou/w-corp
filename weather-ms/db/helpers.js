const connectToDatabase = require('./connect');
const WeatherLocations = require('./models/WeatherLocation');
const Locations = require('./models/Location');
const { getLastHalfHourDate } = require('../utils');

const getCurrentWeatherFromDB = async ({
  location,
}) => {
  await connectToDatabase();
  const lastHalfHourDate = getLastHalfHourDate();
  const result = await WeatherLocations
    .findOne({ date: { $gte: lastHalfHourDate }, location });
  return result && result.toObject();
};

const addCurrentWeatherToDB = async ({
  weather,
  location,
}) => {
  await connectToDatabase();
  const result = await WeatherLocations.create({
    temperature: weather.main.temp,
    date: new Date(),
    location,
  });

  return result;
};

const getLocationMonthlyMisuration = async ({
  month,
  location,
}) => {
  await connectToDatabase();

  const result = await WeatherLocations.aggregate([
    { $addFields: { month: { $month: '$date' } } },
    { $match: { month, location } },
  ]);

  return result;
};

const addLocation = async ({ name, active = true }) => {
  await connectToDatabase();
  const result = await Locations.create({
    name,
    active,
  });

  return result;
};

const getLocationByName = async ({ name }) => {
  await connectToDatabase();
  const result = await Locations.findOne({
    name,
  });

  return result;
};

const getLocations = async () => {
  await connectToDatabase();

  const result = await Locations.find({});
  return result.map((res) => res.toObject());
};

const getActiveLocations = async ({ active }) => {
  await connectToDatabase();

  const result = await Locations.find({ active });
  return result.map((res) => res.toObject());
};

const _getLocationById = async (id) => {
  await connectToDatabase();
  const result = await Locations.findById(id);
  return result;
};

const updateLocation = async ({ id, params }) => {
  await connectToDatabase();
  await Locations.findByIdAndUpdate({ _id: id }, { $set: params });
  const updated = await _getLocationById(id);
  return updated;
};

const deleteLocation = async ({ id }) => {
  await connectToDatabase();
  const result = await Locations.deleteOne({ _id: id });

  return result;
};

module.exports = {
  getCurrentWeatherFromDB,
  addCurrentWeatherToDB,
  getLocationMonthlyMisuration,
  addLocation,
  getLocations,
  updateLocation,
  deleteLocation,
  getLocationByName,
  getActiveLocations,
};
