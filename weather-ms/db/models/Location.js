const mongoose = require('mongoose');

const LocationsSchema = new mongoose.Schema({
  name: String,
  active: Boolean,
});
module.exports = mongoose.model('Locations', LocationsSchema);
