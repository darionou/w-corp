const { convertUnitDegreesTemperature, calculateMonthlyAverage, getMonth } = require('../weather-ms/utils');
const mocksMeasurements = require('./mocks/measurements');

describe('convertUnitDegreesTemperature', () => {
  test('Conversion to Celius', () => {
    const unit = 'C';
    const degrees = 280;
    const degreesConversion = convertUnitDegreesTemperature(degrees, unit);
    expect(parseFloat(degreesConversion)).toBe(7.00);
  });
  test('Conversion to Farenight', () => {
    const unit = 'F';
    const degrees = 273;
    const degreesConversion = convertUnitDegreesTemperature(degrees, unit);
    expect(parseFloat(degreesConversion)).toBe(31.73);
  });
  test('Conversion to Kelvin', () => {
    const degrees = 280;
    const degreesConversion = convertUnitDegreesTemperature(degrees);
    expect(parseFloat(degreesConversion)).toBe(280.00);
  });
});

describe('calculateMonthlyAverage', () => {
  test('Calculate average', () => {
    const average = calculateMonthlyAverage(mocksMeasurements);
    expect(average).toBe(284.17);
  });
});

describe('getMonth', () => {
  test('Get august', () => {
    const month = getMonth('aug');
    expect(month).toBe(8);
  });
  test('Get invalid name', () => {
    const month = getMonth('err');
    expect(month).toBe(undefined);
  });
});
