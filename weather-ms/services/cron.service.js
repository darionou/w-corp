const OpenWeatherMapLib = require('../libs/openWeatherMap');
const { error } = require('../libs/errors');
const { addCurrentWeatherToDB, getActiveLocations } = require('../db/helpers');

const { getLogger } = require('../logger');

const logger = getLogger('cron.svc');

const _getWeatherAndSave = async (location) => {
  const weather = await OpenWeatherMapLib.getWeatherByCityName({ location: location.name });
  return addCurrentWeatherToDB({ weather, location: location.name });
};

const saveMeasurements = async () => {
  logger.info('[getLocations] start');
  const locations = await getActiveLocations({ active: true });
  logger.info('[getLocations] result', { locations });

  const promiseArray = locations.map(
    (location) => _getWeatherAndSave(location),
  );
  await Promise.all(promiseArray);
};

module.exports = {
  saveMeasurements,
};
