/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
const APP_ROOT = '../../weather-ms';

const _ = require('lodash');
const eventGenerator = require('./eventGenerator');

let result;

const viaHandler = async (event, functionName) => {
  const handler = require(`${APP_ROOT}/handler`);
  const context = {};

  const callback = (err, response) => {
    if (err) {
      return err;
    }
    const contentType = _.get(
      response,
      'headers.Content-Type',
      'application/json',
    );
    if (response.body && contentType === 'application/json') {
      response.body = JSON.parse(response.body);
    }

    result = response;
    return response;
  };
  return handler[functionName](event, context, callback);
};

const invokeGetWeather = async location => {
  const event = eventGenerator({
    queryStringObject: {
      location,
    },
  });
  await viaHandler(event, 'getWeatherLive');

  return result;
};

const invokeGetWeatherWithUnit = async (location, unit) => {
  const event = eventGenerator({
    queryStringObject: {
      location,
      unit,
    },
  });
  await viaHandler(event, 'getWeatherLive');

  return result;
};

module.exports = {
  invokeGetWeather,
  invokeGetWeatherWithUnit,
};
