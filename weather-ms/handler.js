const express = require('express');

const bodyParser = require('body-parser');
const serverless = require('serverless-http');
const { getLogger } = require('./logger');

const logger = getLogger('handler');

const locationServices = require('./services/location.service');
const cronServices = require('./services/cron.service');

const { getRequest } = require('./utils');

const weatherServices = require('./services/weather.service');

const app = express();

app.use(bodyParser.json({ strict: false }));

app.get('/weather/live', async (req, res) => {
  try {
    logger.info('[getLiveWeather] start', { req });
    const weather = await weatherServices.getLiveWeather(req);
    logger.info('[getLiveWeather] result', { req });
    res.json(weather);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

const getWeatherLive = async (event, context, callback) => {
  try {
    logger.info('[getWeatherLive] start', { event });
    const req = {
      query: event.queryStringParameters || {},
    };
    const result = await weatherServices.getLiveWeather(req);
    logger.info('[getWeatherLive] result', { result });
    callback(null, {
      statusCode: 200,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(true),
    });
  } catch (error) {
    // logger.error('[getWeatherLive] ', error);
    callback(null, {
      statusCode: error.statusCode || 500,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        error: {
          ...error, message: error.message,
        },
      }),
    });
  }
};

app.get('/weather/live', async (req, res) => {
  try {
    logger.info('[getLiveWeather] start', { req });
    const result = await weatherServices.getLiveWeather(req);
    logger.info('[getLiveWeather] result', { req });
    res.json(result);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

const getWeatherAverage = async (event, context, callback) => {
  try {
    // logger.info('[getWeatherAverage] start', { event });
    const req = {
      query: event.queryStringParameters || {},
    };
    const result = await weatherServices.getWeatherAverage(req);
    // logger.info('[getWeatherAverage] result', { result });
    callback(null, {
      statusCode: 200,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(result),
    });
  } catch (error) {
    logger.error('[getWeatherAverage] ', error);
    callback(null, {
      statusCode: error.statusCode || 500,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        error: {
          ...error, message: error.message,
        },
      }),
    });
  }
};

app.get('/weather/average', async (req, res) => {
  try {
    logger.info('[getWeatherAverage] start', { req });
    const result = await weatherServices.getWeatherAverage(req);
    logger.info('[getWeatherAverage] result', { req });
    res.json(result);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

const addLocation = async (event, context, callback) => {
  try {
    logger.info('[locationCreate] start', { event });
    const req = getRequest(event);
    const result = await locationServices.locationCreate(req);
    logger.info('[locationCreate] result', { result });
    callback(null, {
      statusCode: 200,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(result),
    });
  } catch (error) {
    logger.error('[locationCreate] ', error);
    callback(null, {
      statusCode: error.statusCode || 500,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        error: {
          ...error, message: error.message,
        },
      }),
    });
  }
};

app.post('/locations', async (req, res) => {
  try {
    logger.info('[locationCreate] start', { req });
    const result = await locationServices.locationCreate(req);
    logger.info('[locationCreate] result', { req });
    res.json(result);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

const getLocations = async (event, context, callback) => {
  try {
    logger.info('[locationsRetrieve] start', { event });
    const req = getRequest(event);
    const result = await locationServices.locationsRetrieve(req);
    logger.info('[locationsRetrieve] result', { result });
    callback(null, {
      statusCode: 200,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(result),
    });
  } catch (error) {
    logger.error('[locationsRetrieve] ', error);
    callback(null, {
      statusCode: error.statusCode || 500,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        error: {
          ...error, message: error.message,
        },
      }),
    });
  }
};

app.get('/locations', async (req, res) => {
  try {
    logger.info('[locationsRetrieve] start', { req });
    const result = await locationServices.locationsRetrieve(req);
    logger.info('[locationsRetrieve] result', { req });
    res.json(result);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

const updateLocation = async (event, context, callback) => {
  try {
    logger.info('[locationUpdate] start', { event });
    const req = getRequest(event);
    const result = await locationServices.locationUpdate(req);
    logger.info('[locationUpdate] result', { result });
    callback(null, {
      statusCode: 201,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(result),
    });
  } catch (error) {
    logger.error('[locationUpdate] ', error);
    callback(null, {
      statusCode: error.statusCode || 500,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        error: {
          ...error, message: error.message,
        },
      }),
    });
  }
};

app.put('/locations/:id', async (req, res) => {
  try {
    logger.info('[locationUpdate] start', { req });
    const result = await locationServices.locationUpdate(req);
    logger.info('[locationUpdate] result', { req });
    res.json(result);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

const deleteLocation = async (event, context, callback) => {
  try {
    logger.info('[locationDelete] start', { event });
    const req = getRequest(event);
    const result = await locationServices.locationDelete(req);
    logger.info('[locationDelete] result', { result });
    callback(null, {
      statusCode: 202,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(result),
    });
  } catch (error) {
    logger.error('[locationDelete] ', error);
    callback(null, {
      statusCode: error.statusCode || 500,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        error: {
          ...error, message: error.message,
        },
      }),
    });
  }
};

app.delete('/locations/:id', async (req, res) => {
  try {
    logger.info('[locationDelete] start', { req });
    const result = await locationServices.locationDelete(req);
    logger.info('[locationDelete] result', { req });
    res.json(result);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

const runCron = async (event, context, callback) => {
  try {
    const result = await cronServices.saveMeasurements();
    callback(null, {
      statusCode: 200,
      body: JSON.stringify({
        result,
      }),
    });
  } catch (err) {
    logger.error('[HJ]', err);
    callback(null, {
      statusCode: 500,
      body: JSON.stringify({ error: { message: err.message } }),
    });
  }
};

app.get('/cron/check', async (req, res) => {
  try {
    const result = await cronServices.saveMeasurements();
    logger.info('[saveMeasurements] result', { req });
    res.json(result);
  } catch (error) {
    res.status(error.statusCode || 500).json({
      error: {
        ...error, message: error.message,
      },
    });
  }
});

module.exports = {
  app: serverless(app),
  getWeatherAverage,
  getWeatherLive,
  addLocation,
  getLocations,
  updateLocation,
  deleteLocation,
  runCron,
};
