# W-Corp


This repository illustrates a simple way to retrieve and store weather data.


## Setup


- Install Node 

- Install serverless 
````
$ npm i -g serverless 
````
- The src folder is weather-ms 
````
$ cd weather-ms/
````

- Install node modules
````
$ npm i 
````
- Initialize env variables file
````
$ touch myEnvironment.yml and uncomment the file serverless.yml environment
````
- Run tests
````
$ npm test
$ npm test-integration
````



## Running locally
- Start serverless offline
````
$ sls offline
````





