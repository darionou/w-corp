const { error } = require('../libs/errors');
const {
  addLocation, getLocations, updateLocation, deleteLocation, getLocationByName,
} = require('../db/helpers');

const _locationAlreadyExists = async ({ name }) => {
  const location = await getLocationByName({ name });
  return !!location;
};

const locationCreate = async (req) => {
  const { name, active } = req.body;

  if (!name) throw error('MN');
  const exists = await _locationAlreadyExists({ name });
  if (exists) throw error('AE');

  const result = await addLocation({ name, active });
  return result;
};
const locationsRetrieve = async (req) => {
  const result = await getLocations();
  return result;
};

const locationUpdate = async (req) => {
  const params = req.body;
  const { id } = req.params;
  const result = await updateLocation({ id, params });

  return result;
};
const locationDelete = async (req) => {
  const { id } = req.params;
  await deleteLocation({ id });

  return {};
};

module.exports = {
  locationCreate,
  locationsRetrieve,
  locationUpdate,
  locationDelete,
};
