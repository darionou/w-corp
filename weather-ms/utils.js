const monthDictionary = {
  jan: 1,
  feb: 2,
  mar: 3,
  apr: 4,
  may: 5,
  jun: 6,
  jul: 7,
  aug: 8,
  sep: 9,
  oct: 10,
  nov: 11,
  dec: 12,
};

const getRequest = (event) => ({
  query: event.queryStringParameters || {},
  requestContext: event.requestContext || {},
  headers: event.headers || {},
  params: event.pathParameters || {},
  body: typeof event.body === 'string' ? JSON.parse(event.body) : event.body,
});

const getMonth = (month) => monthDictionary[month];
const getLastHalfHourDate = () => new Date(new Date().setMinutes(new Date().getMinutes() - 30));
const round = (number) => Number(parseFloat(number).toFixed(2));

const convertUnitDegreesTemperature = (degrees, unit = 'K') => {
  switch (unit) {
    case 'F':
      return round(degrees * 1.8 - 459.67);
    case 'C':
      return round(degrees - 273);
    default:
      break;
  }

  return round(degrees);
};

const calculateMonthlyAverage = (monthlyData) => monthlyData.reduce(
  (acc, curr) => acc + curr.temperature, 0,
) / monthlyData.length;

module.exports = {
  getMonth,
  getLastHalfHourDate,
  calculateMonthlyAverage,
  convertUnitDegreesTemperature,
  getRequest,
};
