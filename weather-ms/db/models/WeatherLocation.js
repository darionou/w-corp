const mongoose = require('mongoose');

const WeatherLocationsSchema = new mongoose.Schema({
  date: Date,
  location: String,
  temperature: Number,
});
module.exports = mongoose.model('WeatherLocations', WeatherLocationsSchema);
