const { getLogger } = require('../logger');

const logger = getLogger('errors.lib');

const errors = {
  ML: { message: 'Missing parameter - location' },
  MM: { message: 'Missing parameter - month' },
  IM: { message: 'Invalid parameter - month' },
  MT: { message: 'Third part api error' },
  MN: { message: 'Missing parameter - name' },
  AE: { message: 'Location already exists ' },
};

const handleError = ({ code, detail, statusCode }) => {
  const err = errors[code];
  const message = err ? err.message : '';

  const errorResult = new Error(message || code);
  if (code !== errorResult.message) {
    errorResult.code = code;
  }
  errorResult.statusCode = (err && err.statusCode) ? err.statusCode : statusCode;
  if (detail) {
    errorResult.detail = detail;
    if (!errorResult.message) errorResult.message = detail;
  }
  logger.error('[LE]', errorResult);
  return errorResult;
};

const error = (code, detail, statusCode = 400) => handleError({ code, detail, statusCode });
const fatalError = (code, detail, statusCode = 500) => handleError({ code, detail, statusCode });

exports.error = error;
exports.fatalError = fatalError;
